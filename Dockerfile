FROM ubuntu:20.04
RUN apt-get update && apt-get -y install --no-install-recommends python3 python3-pip


ENV ROOT=/app
WORKDIR ${ROOT}
COPY . ${ROOT}/

RUN pip install --upgrade pip
RUN pip install -r requirements.txt

CMD ["/bin/bash", "-c", "coverage run --branch -m unittest test.py && coverage json"]